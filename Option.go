package cHTTPClient

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type HTTPMethod string

const (
	MethodHEAD    HTTPMethod = "HEAD"
	MethodGET     HTTPMethod = "GET"
	MethodPOST    HTTPMethod = "POST"
	MethodOPTIONS HTTPMethod = "OPTIONS"
	MethodDELETE  HTTPMethod = "DELETE"
	MethodPUT     HTTPMethod = "PUT"
	MethodPATCH   HTTPMethod = "PATCH"
	MethodTRACE   HTTPMethod = "TRACE"
	MethodCONNECT HTTPMethod = "CONNECT"
)

const (
	HeaderContentType     = "Content-Type"
	HeaderContentTypeJson = "application/json"
)

type Option struct {
	Host           string            // 域名
	Uri            string            // 路由
	Method         HTTPMethod        // 方法
	Timeout        time.Duration     // 总超时时间
	ConnectTimeout time.Duration     // 连接超时时间
	Try            int               // 重试次数, 小于 0 则永久重试
	Interval       int               // 重试间隔, 每间隔 interval 秒进行一次请求
	IntervalIncr   int               // 重试间隔递增值, 间隔时间从 interval 开始, 每次重试都会递增 interval_incr 秒
	IntervalTry    int               // 重试间隔递增请求次数阈值, 每次递增后尝试请求 interval_try 次, 再执行下一次递增
	IntervalMax    int               // 重试间隔最大值, 超过该值则间隔时间不再递增
	Headers        map[string]string // 请求头
	Query          map[string]string // 路由参数
	Data           any               // POST 参数

	Stream          chan string                                                             `json:"-"`
	ResponseHandler func(ctx *gin.Context, response *http.Response) (retry bool, err error) `json:"-"`
}

type Response struct {
	Header     http.Header
	Status     string
	StatusCode int
	Body       []byte `json:"-"`
}
