package cHTTPClient

import (
	"github.com/gin-gonic/gin"

	"gitee.com/csingo/cLog"
)

func Request(ctx *gin.Context, option Option) (response *Response, err error) {
	var body, msg string

	cLog.WithContext(ctx, map[string]interface{}{
		"source": "cHTTPClient.Request",
		"option": option,
	}).Trace("外部请求参数")
	defer func() {
		if err != nil {
			msg = err.Error()
		}
		cLog.WithContext(ctx, map[string]interface{}{
			"source": "cHTTPClient.Request",
			"err":    msg,
			"result": response,
			"body":   body,
		}).Trace("外部请求返回")
	}()

	cli := newClient(ctx, &option)
	response, err = cli.send()

	if response != nil && len(response.Body) > 0 {
		body = string(response.Body)
	}

	return
}
