package cHTTPClient

import (
	"testing"

	"gitee.com/csingo/cContext"
)

func TestRequest(t *testing.T) {
	ctx := cContext.New()
	cli := newClient(ctx, &Option{
		Try:          10,
		Interval:     10,
		IntervalIncr: 2,
		IntervalTry:  2,
		IntervalMax:  100,
	})
	_, _ = cli.send()
}
